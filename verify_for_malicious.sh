if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <safe_directory> <filename_to_be_checked>"
    exit 1
fi

safe_directory=$1
filename_to_be_checked=$2

if grep -q -i -e 'danger' -e 'risk' -e 'attack' -e 'virus' -e 'corrupted' -e 'malware' "$filename_to_be_checked"; then
    if [ -f "$filename_to_be_checked" ]; then
        num_lines=$(wc -l < "$filename_to_be_checked")
        num_words=$(wc -w < "$filename_to_be_checked")
        char_count=$(wc -m < "$filename_to_be_checked")

        if [ "$num_lines" -lt 3 ] && [ "$num_words" -gt 1000 ] && [ "$char_count" -gt 2000 ]; then
            echo $filename_to_be_checked
        else
            echo "SAFE"
        fi
    fi
fi
