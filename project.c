#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <unistd.h>
#include <time.h>
#include <sys/wait.h>

#define BUFFER_SIZE 1024

int child[10];
int child_count = 0;
char safe_directory[256];

char* search_for_source(DIR* dir, char* directory) {
  char* file = (char*)malloc(sizeof(char*));
  struct dirent* dir_info;
  char* snapshot = (char*)malloc(sizeof(char*) * 10);
  sprintf(snapshot, "%s/data_source.txt", directory);

  while((dir_info = readdir(dir)) != NULL) {
    sprintf(file, "%s", dir_info->d_name);
    if(strcmp(file, "data_source.txt") == 0) {
      printf("The source data file exists!\n");
      return snapshot;
    }
  }
  printf("The sorce data file does NOT exist!\n It will now be created!\n");
  int creat = open(snapshot, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR, S_IWUSR);
  if (creat == -1) {
    perror("Error creating snapshot file");
    close(creat);
    exit(EXIT_FAILURE);
}
  close(creat);
  closedir(dir);
  return snapshot;
}

void check_data(struct stat buf, char* snap, char* file_name, char* relpath) {
  int snap_file = open(snap, O_RDWR);
  printf("Snap:%s\n", snap);
  if(snap_file == -1) {
    printf("Error opening file!\n");
    close(snap_file);
    exit(EXIT_FAILURE);
  }
  char buffer[BUFFER_SIZE];
  char write_data[BUFFER_SIZE];
  char comparing_data[BUFFER_SIZE];
  ssize_t bytes_read;

  char permissions[10];
  permissions[0] = (buf.st_mode & S_IRUSR) ? 'r' : '-';
  permissions[1] = (buf.st_mode & S_IWUSR) ? 'w' : '-';
  permissions[2] = (buf.st_mode & S_IXUSR) ? 'x' : '-';
  permissions[3] = (buf.st_mode & S_IRGRP) ? 'r' : '-';
  permissions[4] = (buf.st_mode & S_IWGRP) ? 'w' : '-';
  permissions[5] = (buf.st_mode & S_IXGRP) ? 'x' : '-';
  permissions[6] = (buf.st_mode & S_IROTH) ? 'r' : '-';
  permissions[7] = (buf.st_mode & S_IWOTH) ? 'w' : '-';
  permissions[8] = (buf.st_mode & S_IXOTH) ? 'x' : '-';
  permissions[9] = '\0';

  int danger = 0;
  for(int i = 0; i < 10; ++i) {
    if(permissions[i] == '-') {
      danger++;
    }
  }
  if(danger == 9){
    char cwd[256];
    getcwd(cwd, sizeof(cwd));
    char command[1124];
    sprintf(command, "%s/%s %s/%s %s", cwd, "verify_for_malicious.sh", cwd, "safe_directory", relpath);
    printf("Command is: %s\n", command);

    int fd[2];
    if (pipe(fd) == -1) {
        perror("pipe");
        exit(EXIT_FAILURE);
    }

    pid_t pid = fork();
    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    }

    if (pid == 0) {
        close(fd[0]);

        dup2(fd[1], STDOUT_FILENO);
        close(fd[1]);

        execl("/bin/bash", "/bin/bash", "-c", command, NULL);
        perror("execl");
        exit(EXIT_FAILURE);
    } else {
        close(fd[1]);

        char buffer[1024];
        ssize_t count;
        while ((count = read(fd[0], buffer, sizeof(buffer)-1)) > 0) {
            buffer[count] = '\0';
            printf("Received from child: %s", buffer);
        }
        if(buffer[0] != 'S' && buffer[1] != 'A' && buffer[2] != 'F' && buffer[3] != 'E') {
          char command[1024];
          sprintf(command, "mv %s %s/%s", relpath, cwd, safe_directory);
          printf("COMM:%s\nBuffer:%s\n", command, buffer);
          system(command);
        }
        
        close(fd[0]);

        wait(NULL);
    }
    return;
  }

  sprintf(write_data, "Nume fisier: %s\n", file_name);
  sprintf(write_data, "%sDimensiune: %ld\n", write_data, buf.st_size);
  sprintf(write_data, "%sUltima modificare: %s", write_data, ctime(&buf.st_mtime));
  sprintf(write_data, "%sNumar de legaturi: %ld\n", write_data, buf.st_nlink);
  sprintf(write_data, "%sFile inode: %ld\n", write_data, buf.st_ino); 
  sprintf(write_data, "%sPermisiuni: %s\n\n", write_data, permissions);
  ssize_t bytes_to_write = strlen(write_data);

  bytes_read = read(snap_file, &buffer, BUFFER_SIZE);
  if (bytes_read == -1) {
    perror("Error reading file");
    close(snap_file);
    exit(EXIT_FAILURE);
  }
  buffer[bytes_read] = '\0';

  char* is_file = strstr(buffer, file_name);
  if(is_file != NULL) {
    printf("We have data about the file!\n");
    long position = is_file - buffer - 13;
    printf("Comparing!\n\n");
    char* next = strstr(is_file, "\nNume");
    long next_file_data = 0;

    if(next == NULL)
      next_file_data = bytes_read;
    else
      next_file_data = next - buffer;

    printf("'%s' found at: %ld\n\n", file_name, position);
    printf("Next file data: %ld\n", next_file_data);
    if(lseek(snap_file, position, SEEK_SET) != -1) {
      bytes_read = read(snap_file, &comparing_data, next_file_data - position + 1);
      printf("Bits read:%zd\n", bytes_read);
      comparing_data[bytes_read] = '\0';
      if(strcmp(write_data, comparing_data) != 0) {
        printf("File modified!\n");
        int index = 0;
        printf("Vechi:\n");
        for(int i = position; i < next_file_data; ++i) {
            printf("%c ", buffer[i]);
            buffer[i] = write_data[index];
            ++index;
        }
        index = 0;
        printf("Nou:\n");
         for(int i = position; i < next_file_data; ++i) {
            printf("%c ", write_data[index]);
            ++index;
        }
        lseek(snap_file, position, SEEK_SET);
        write(snap_file, &write_data, index);
      }
      else {
        printf("File not modified!\n");
      }
    }
  }
  else {
    printf("We don't have data about the file! Add!\n\n");
    write(snap_file, &write_data, bytes_to_write);
  }

  close(snap_file);
}

int iter_dir(DIR* dir, char* path, char* snap) {
  char* file = (char*)malloc(sizeof(char*));
  struct dirent* dir_info;
  struct stat buf;
  int number_of_files = 0;
  
  while((dir_info = readdir(dir)) != NULL) {
    char* relpath = (char*)malloc(sizeof(char*) * 10);
    sprintf(file, "%s", dir_info->d_name);
    sprintf(relpath, "%s/%s", path, file);
    
    stat(relpath, &buf);

    if((S_ISDIR(buf.st_mode) == 1) && (strcmp(file, ".") != 0 && strcmp(file, "..") != 0)) {
      DIR* new_dir;
      if((new_dir = opendir(file)) != NULL) {
        number_of_files++;
        iter_dir(new_dir, relpath, snap);
      }
    }
    else if(strcmp(file, ".") != 0 && strcmp(file, "..") != 0) {
      if(strcmp(file, "data_source.txt") != 0)
        check_data(buf, snap, file, relpath);
      number_of_files++;
    }
  }
  closedir(dir);
  return number_of_files;
}

int move(char *source, char *target) {
  int status = 0;
  FILE *fp = NULL;
  char command[999];
  char* new_file_name = (char*)malloc(sizeof(char*) * 10);
  char* folder = (char*)malloc(sizeof(char*) * 10);
  for(int i = 0; i < strlen(source); ++i) {
    if(source[i] == '/')
      break;
    folder[i] = source[i];
  }
  printf("Folder: %s\n", folder);
  sprintf(new_file_name, "data_source_%s.txt", folder);
  sprintf(command, "mv %s %s", source, new_file_name);
  system(command);
  sprintf(command, "mv %s %s", new_file_name, target);
  if ((fp=popen(command,"r")) != NULL) {
    pclose(fp); 
    status = 1; 
  }
  return status;
}      

void wait_finish() {
    int status;
    for(int i = 0; i < child_count; i++) {
        waitpid(child[i], &status, 0);
        if (WIFEXITED(status)) {
            int exit_status = WEXITSTATUS(status);
            printf("Child %d exited with status %d\n", child[i], exit_status);
        }
    }
}

int main(int argc, char **argv) {
  if(argc <= 10) {
    int option = 1;
    if(strcmp(argv[1], "-o") == 0) {
        printf("Redirectam iesirea!\n");
        option = 3;
    }
    if(strcmp(argv[3], "-s") == 0) {
        printf("Verificam pentru virusi!\n");
        strcpy(safe_directory, argv[4]);
        option = 5;
    }
    printf("Avem de parcurs %d directoare!\n", argc - option);
    for(int i = option; i < argc; ++i) {
        char* file_to_open = argv[i];
        DIR* dir = opendir(file_to_open);
        char* snap = search_for_source(dir, file_to_open);
        if(option == 3) {
            if(move(snap, argv[2]) == 1)
                printf("Snapshot file moved successfully!\n");
            else
                printf("Snapshot file was not moved!\n");
        }
        int id = fork();
        if(id == 0) {
            dir = opendir(file_to_open);
            iter_dir(dir, file_to_open, snap);
            exit(0);
        }
        else {
            child[i] = id;
            child_count++;
        }
    }
  }
  wait_finish();
  return 0;
}
